#include <stdio.h>
#include <locale.h>

void bubbleSort(int v[], int n);

void bubbleSort(int v[], int n){
    int continua, maior;
    do{
        continua = 0;
        for (int i = 0; i < n - 1; i++) {
            if(v[i] > v[i + 1]){
                maior = v[i];
                v[i] = v[i + 1];
                v[i + 1] = maior;
                continua = i;
            }
        }
        n--;
    }while (continua != 0);
}

void imprimeVetor(int v[], int n);

void imprimeVetor(int v[], int n){
    printf("\nExibe vetor n�o ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

void imprimeVetorOrdenado(int [], int n);

void imprimeVetorOrdenado(int v[], int n){

    bubbleSort(v, n);
    printf("\nExibe vetor ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

int main() {
    setlocale(LC_ALL, "Portuguese");
    int vetor1[6] = {2, 1, 4, 3, 6, 5};
    int vetor2[10] = {2, 3, 8, -4, 7, 10, -9, 1, 3, 6, 5};

    imprimeVetor(vetor1, 6);
    imprimeVetorOrdenado(vetor1, 6);
    imprimeVetor(vetor2, 10);
    imprimeVetorOrdenado(vetor2, 10);

    return 0;
}
